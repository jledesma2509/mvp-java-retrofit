package com.jledesma.myapplication.view.category;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jledesma.myapplication.R;
import com.jledesma.myapplication.contract.CategoryContract;
import com.jledesma.myapplication.model.Category;
import com.jledesma.myapplication.presenter.CategoryPresenter;

public class CategoryActivity extends AppCompatActivity implements CategoryContract.View {

    CategoryContract.Presenter categoryPresenter;
    SweetAlertDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        categoryPresenter = new CategoryPresenter(this);

    }

    @OnClick(R.id.btnObtenerCategoria)
    public void onClickCategory(){

        getCategory();
    }

    @Override
    public void showLoadingView() {

        pd = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
        pd.getProgressHelper().setBarColor(Color.parseColor("#102670"));
        pd.setContentText("Por favor, espere...");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    public void hideLoadingView() {

        pd.dismiss();
    }

    @Override
    public void showError(String error) {

        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCategory(Category category) {

        Toast.makeText(this,category.getNombre(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCategory() {

        categoryPresenter.categories();
    }
}
