package com.jledesma.myapplication.data;

import android.content.Context;

import com.jledesma.myapplication.contract.CategoryContract;
import com.jledesma.myapplication.model.Category;
import com.jledesma.myapplication.presenter.CategoryPresenter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryRepository {


    public CategoryRepository(CategoryPresenter categoryPresenter) {

    }

    public void logIn(final OperationCallback callback) {

        EndPoint endPoint = Api.getConfiguration().create(EndPoint.class);
        Call<Category> response = endPoint.obtenerCategoria("5d8bdd33bb99b20400d44c6c");
        response.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {

                if (response.isSuccessful()){

                    Category category = response.body();
                    callback.onSuccess(category);
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {

                callback.onError(t.getMessage());
            }
        });

    }


}
