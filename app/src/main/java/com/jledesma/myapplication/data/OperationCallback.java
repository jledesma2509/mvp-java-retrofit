package com.jledesma.myapplication.data;

public interface OperationCallback {

    void onSuccess(Object obj);
    void onError(Object obj);
}
