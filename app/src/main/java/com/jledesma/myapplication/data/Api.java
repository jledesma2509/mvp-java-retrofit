package com.jledesma.myapplication.data;

import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    public static Retrofit getConfiguration(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://stormy-mountain-84723.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;

    }

}
