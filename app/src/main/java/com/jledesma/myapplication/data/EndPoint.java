package com.jledesma.myapplication.data;

import com.jledesma.myapplication.model.Category;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface EndPoint {


    @GET("categoria/{idCategoria}")
    @Headers("Content-Type:application/json")
    Call<Category> obtenerCategoria(@Path("idCategoria") String idCategoria);

}
