package com.jledesma.myapplication.contract;

import com.jledesma.myapplication.model.Category;

public interface CategoryContract {

    interface View{
        void showLoadingView();
        void hideLoadingView();
        void showError(String error);
        void showCategory(Category category);
        void getCategory();
    }

    interface Presenter{
        void categories();
    }
}
