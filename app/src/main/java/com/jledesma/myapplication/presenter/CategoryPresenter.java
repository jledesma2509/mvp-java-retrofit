package com.jledesma.myapplication.presenter;

import com.jledesma.myapplication.contract.CategoryContract;
import com.jledesma.myapplication.data.CategoryRepository;
import com.jledesma.myapplication.data.OperationCallback;
import com.jledesma.myapplication.model.Category;
import com.jledesma.myapplication.view.category.CategoryActivity;

public class CategoryPresenter implements CategoryContract.Presenter  {

    CategoryContract.View view;
    CategoryRepository repository;

    public CategoryPresenter(CategoryContract.View view) {

        this.view = view;
        repository = new CategoryRepository(this);
    }

    @Override
    public void categories() {

        view.showLoadingView();
        repository.logIn(new OperationCallback() {

            @Override
            public void onSuccess(Object obj) {
                view.hideLoadingView();
                view.showCategory((Category) obj);
            }

            @Override
            public void onError(Object obj) {
                view.hideLoadingView();
                view.showError(obj.toString());
            }
        });
    }
}
